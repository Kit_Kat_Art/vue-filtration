import $ from 'jquery';


function DropDown(el) {

    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.drop li');
    this.val = '';
    this.index = -1;
    this.initEvents();

}
DropDown.prototype = {
    initEvents: function () {
        var obj = this;
        obj.dd.on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).toggleClass('active');
        });
        obj.opts.on('click', function () {
            var opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);

            obj.name = opt.attr("name");
            opt.siblings().removeClass('selected');
            opt.filter(':contains("' + obj.val + '")').addClass('selected');


            //  console.log(obj.dd[0].getAttribute("name"));
            var id = parseInt(obj.dd[0].getAttribute("id"));
            //    console.log(obj.val);
            //   document.getElementById(id + 1 + '_disable').setAttribute("id", id + 1);


            document.getElementById('input_' + id + '').value = obj.val;


        }).change();
    },
    getValue: function () {
        return this.val;
    },
    getIndex: function () {
        return this.index;
    }

};
$(function () {
    var dd1 = new DropDown($('#1'));
    var dd2 = new DropDown($('#2'));
    var dd3 = new DropDown($('#3'));

    $(document).click(function () {
        $('.wrap-drop').removeClass('active');

    });
});
